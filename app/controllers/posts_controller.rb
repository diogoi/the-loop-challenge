class PostsController < ApplicationController
    before_action :authorize

    def show
        @posts = Post.where('title like ?', "%#{params[:search]}%")
        render 'index'
    end

    def index
        # @posts = Post.all
        @posts = Post.paginate(page: params[:page])
    end
    def new
        @post = Post.new
    end

    def create
        @post = Post.new (post_params)
        @post.user = current_user
        if @post.save
            redirect_to posts_path
        else
            render 'new'
        end
    end

    def destroy
        @post = Post.find(params[:id])
        @post.destroy
      
        redirect_to posts_path
    end

    private 
        def post_params
            params.require(:post).permit(:title, :body)
        end
end
