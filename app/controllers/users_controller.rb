class UsersController < ApplicationController
  before_action :authorize, except: [:new, :create]
  def new
    @user = User.new
  end

  def show
    @user = current_user
    render 'edit'
  end

  def create
    @user = User.new (user_params)
    if @user.save
      sign_in @user
      redirect_to posts_path
    else
      render 'new'
    end
  end


  def update
    @user = current_user

    if(params[:user][:current_password] == "")
      @user.errors.add(:current_password, "can't be blank")
      render 'edit'
      return
    end

    #Try to authenticate a user with the parameter "current_password"
    if @user && @user.authenticate(params[:user][:current_password])

      #Check if the user wants to change the password
      if(params[:user][:password] == "")
        params_without_password = user_params.except(:password, :password_confirmation)
        @user.update_attributes(params_without_password)
        redirect_to posts_path
        return
      
      #Check if password and password_confirmation match
      else
        if(params[:user][:password] == params[:user][:password_confirmation])
          @user.update_attributes(user_params)
        else 
          @user.errors.add(:password, "and Password confirmation don't match")
        end
      end
    else
      @user.errors.add(:current_password, "is invalid")
    end

    redirect_to posts_path
  end

  def edit
    @user = current_user
  end

  def destroy
    @user = current_user
    session[:user_id] = nil
    @user.destroy
    redirect_to '/'
  end

  private
  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
