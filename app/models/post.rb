class Post < ApplicationRecord

    self.per_page = 5

    belongs_to :user

    validates :title,  presence: true
    validates :body,  presence: true
    validates :user, presence: true
end
