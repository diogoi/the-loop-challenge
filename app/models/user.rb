class User < ApplicationRecord

  attr_accessor :remember_token
  
  has_many :posts, dependent: :destroy

  has_secure_password
  validates :password, presence: true, length: {minimum: 6}
  
  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validates :email, presence: true, format: { with: EMAIL_REGEX}, uniqueness: {case_sensitive: false}

  validates :username, presence: true, uniqueness: {case_sensitive: false}

  before_save { self.email = email.downcase }
  before_save { self.username = username.downcase }

  def new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = self.new_token
    self.update(remember_digest: User.digest(self.remember_token))
  end

  def forget
    self.update(remember_digest: nil)
  end

end
