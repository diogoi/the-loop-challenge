Rails.application.routes.draw do
  resources :users
  resources :posts

  root 'sessions#new'
  get    'sign_in'   => 'sessions#new'
  post   'sign_in'   => 'sessions#create'
  delete 'sign_out'  => 'sessions#destroy'

end
