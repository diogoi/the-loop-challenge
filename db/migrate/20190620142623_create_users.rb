class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username
      t.string :email, null: false, index: {unique: true}
      t.string :password_digest, null: false
      t.string  :remember_digest

      t.timestamps
    end
  end
end
